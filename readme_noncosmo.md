# Data challenge (Readme for non-cosmologists)

While the full info about the challenge can be found in the main [README](README.md), this page is supposed to help participants that are not familiar with N-body simulations and cosmology in general. 

## Description of the dataset 

The dataset is composed of mock galaxy catalogs, that is, simulated distributions of galaxies inside a cubic box, resembling our Universe, where each file contains the 3-dimensional position and velocity of each galaxy.

The distribution of galaxies is simulated through several steps:

-	A set of (chunks) of dark matter particles is distributed on a mesh grid in a cubic box drawing from a Gaussian (or slightly non-Gaussian, see description of the challenge below) probability distribution at each grid position. This represents the initial conditions of the Universe and it is the starting snapshot of the simulation.
-	Each particle is approximately $`100`$ billion times the mass of the sun, so very far from being a particle. This is because we simulate the Universe at very large scales, so, in some sense, we provide a very coarse-grained description of the Universe. This is good enough to describe the statistical distribution of galaxies.
-	These particles are numerically evolved following the dynamical equations of gravity from the initial time to the present day. What is solved is the evolution of a self-gravitating collisionless N-body system in a box with periodic boundary conditions. 
- Quantities are given in comoving coordinates, since the universe is expanding. This means that the physical position $`x_{\rm phys}`$ of each particle is written as $`x_{\rm phys} = a(t) x_{\rm comoving}`$, where $`a (t)`$ is the scale factor describing the expansion, $`x_{\rm comoving}`$ is the comoving coordinate. 
-	Snapshots of the particle positions and velocities can be taken at any time during the evolution. The simulation starts with $`a(t_{\rm ini}) = 0.01`$, we save the snapshot at $`a(t_{\rm challenge}) = 0.5`$.
-	At the time $`t_{\rm challenge}`$, we identify groups of particles that have decoupled from the expansion and collapsed into a compact object (halo).  This identification is based on finding groups of particles that are closed to each other (so called Friends-of-Friends algorithm) whose local density is higher than $`200`$ times the background density. All the halos at this time are classified by their mass, position and velocities.
-	Using a _Halo Occupation Distribution_ algorithm, we assign (or not) a central galaxy to the halo, and if it is a very large halo, we assign satellite galaxies as well, which orbit around the halo center. The algorithm is explained in the master page.

