![](https://gitlab.com/mbiagetti/persistent_homology_lss/-/raw/master/images/eos.png)

# Data Challenge

As part of the IFPU focus week titled [`Interpretable and Higher Order Summary Statistics for Late Time Cosmology`](https://indico.sissa.it/event/77/), taking place in Trieste from 27th June to 1st July 2022, we propose a Data Challenge to brainstorm new ideas for analyzing cosmological data.
The challenge involves identifying the one mock galaxy catalog out of 500 that was run with non-Gaussian initial conditions.

The catalogs can be found [here](https://mega.nz/folder/jMhQ3bBJ#sF5AJIa22Vj_C8zGOQLCow).

You can [subscribe](https://join.slack.com/t/ihosdatachallenge/shared_invite/zt-1bk0ziwaf-9OqU4zoOfRRySLtJSv9hqQ) to the Slack channel of the challenge to discuss intermediate progress, raise issues and share results.

## Description of the dataset

Here we describe the dataset. For non-cosmologists, we have created a separate description, found in the repository file `readme_noncosmo.md`.

### N-body simulations

The datasets are generated from N-body simulations that are part of the Quijote suite of simulations (full info [here](https://quijote-simulations.readthedocs.io/en/latest/)). The initial particle displacements is generated using 2nd order Lagrangian Perturbation Theory (2LPT). The cosmology is flat $`\Lambda`$CDM with $` \sigma_8 = 0.834\,\, h = 0.6711\,\,\Omega_m = 0.3175`$. Initial conditions are generated at $`z_{in} = 63`$ displacing particles according to 2LPT. The code [`Gadget3`](https://wwwmpa.mpa-garching.mpg.de/gadget/) is used to evolve $`512^3`$ particles in a cubic box of $`1`$ Gpc$`/h`$ per side with periodic boundary conditions. Each box has $`500`$ different realizations, i.e. their initial conditions are set with a different random seed. 

### Halos

For our analysis, we find halos in each simulation identifying candidate halos with a minimum of 50 particles using a Friends-of-Friends (FoF) algorithm at redshift $`z = 0.5`$.

### Galaxies

The galaxy-halo relation is assigned using a Halo Occupation Distribution (HOD) algorithm, and galaxies resemble CMASS galaxies from the BOSS survey.
We use the model of [_Zheng et al. (2007)_](https://iopscience.iop.org/article/10.1086/521074/pdf) with parameters adapted to fit the galaxy redshift power spectrum measurements of CMASS galaxies from [_Gil Marin et al. (2016)_](https://ui.adsabs.harvard.edu/abs/2016MNRAS.460.4188G/abstract).
The mean number of galaxies in a halo is given by 

```math
\langle N_{\rm gal} \rangle = \langle N_{\rm cen} \rangle + \langle N_{\rm sat} \rangle
```
with mean central galaxy occupation

```math
 \langle N_{\rm cen} \rangle = \frac 12 \left[ 1+ {\rm erf}\left(\frac{\log M_h - \log M_{\rm min}}{\sigma_{\log M}}\right)\right]
```

and mean satellite galaxy occupation

```math
\langle N_{\rm sat} \rangle = \langle N_{\rm cen} \rangle \left(\frac{M_h - M_0}{M_1}\right)^\alpha
```

The mean number of centrals goes from 0 to 1 for halos with a mass larger than the minimum mass $`
M_{\rm min}`$. The parameter regulating the transition is $`\sigma_{\log M}`$, which parametrizes the scatter between stellar
mass/luminosity and halo mass. The number of satellites around each central is empyrically described by a power law with slope $`\alpha`$ and a basis which depends on the halo mass cut-off for satellite occupation, $`M_0`$ and $`M_h = M_0 + M_1`$ is the typical mass scale for halos
to host one satellite galaxy. Central galaxies are positioned at the center of the halo and are drawn a Bernoulli distribution with $`\langle N_{\rm cen}\rangle`$ as mean. Satellite galaxies are positioned by sampling the NFW profile that depends on the concentration parameter and viral radius of each central halo, as described in [_Robotham & Howlett (2018)_](https://iopscience.iop.org/article/10.3847/2515-5172/aacc70) and drawn from a Poisson distribution with $`\langle N_{\rm sat}\rangle`$ as mean.

For the fiducial parameters of our HOD model, we use the following values:
```math
\{\log M_{\rm min}, \sigma_{\log M}, \log M_0, \alpha, \log M_1\} = \{13.0, 0.2, 13.1, 0.75, 14.25\}. 
```

Galaxy positions are given in redshift space, where the displacement is calculated as

```math
\mathbf{s} = \mathbf{x} + \frac{v_z}{\mathcal H}\, \hat n
```
where $`v_z`$ is the peculiar velocity along the z-axis, $`\mathcal H`$ the Hubble parameter and $`\hat n`$ the direction along the line of sight, which is taken to be the z-axis. The center of the box is `[500,500,500]`. **Note that galaxies have already been displaced in redshift space**.

> The code implementing this HOD algorithm has been developed by Juan Calles and is freely available [here](https://gitlab.com/jcallesh/hod). 

## Primordial non-Gaussianity

Primordial non-Gaussianity is the observational signature of primordial interactions taking place during inflation, and therefore an essential observational window into the particle content and micro-physics of inflation.

The challenge involves identifying whether the simulations are run with Gaussian or non-Gaussian initial conditions. The algorithm, and code, we use to set non-Gaussian initial conditions is explained in [_Scoccimarro et al. (2012)_](https://journals.aps.org/prd/pdf/10.1103/PhysRevD.85.083002).

Simply put, while for Gaussian initial conditions particles are assigned to the initial mesh grid with a value of the density field randomly drawn from a Gaussian distribution at each position, in the case of non-Gaussian initial conditions the distribution has a non-zero value of the three-point correlation function, so-called bispectrum in Fourier space. 

We run initial conditions with two different types of non-Gaussianities, so-called `local` and `equilateral`. The local shape typically peaks on squeezed triangle configurations of the bispectrum, i.e. when correlating a long-wavelength mode with short-wavelength modes. This parameterizes a physical correlation between small-scale physics and the large-scale gravitational potential, induced during inflation. The bispectrum reads

```math
B_\zeta^{\rm local}(k_1,k_2,k_3)  = \frac{6}{5} f_{\rm NL}^{\rm loc} \left[P_\zeta(k_1)P_\zeta(k_2)+2\,\,{\rm perms}\,\right],
```
where $`\zeta`$ can be thought of as the gravitational potential.

The equilateral type is characterized by a bispectrum that peaks in the equilateral configuration, i.e.\ when all sides of the triangle are equal. The bispectrum for equilateral non-Gaussianity is 
```math
 B_\zeta^{\rm equi}(k_1,k_2,k_3) = \frac{18}{5} f_{\rm NL}^{\rm equi}\left[\left(-P_\zeta(k_1)P_\zeta(k_2)-2\,\,{\rm perms}\right) - 2P_\zeta^{2/3}(k_1)P_\zeta^{2/3}(k_2)P_\zeta^{2/3}(k_3)+
 \left(P_\zeta^{1/3}(k_1)P_\zeta^{2/3}(k_2)P_\zeta(k_3)+5\,\,{\rm perms}\right)\right].
```
Here the normalization of the amplitude $`f_{\rm NL}`$ is fixed such that $`f_{\rm NL}^{\rm equi} \equiv f_{\rm NL}^{\rm loc}`$ in the equilateral limit.

## The Challenge

This section will be updated as we move to new challenges of increasing difficulties.

### Some context

Primordial non-Gaussianity, as currently constrained by observations of the Cosmic Microwave Background (CMB) and of the Large Scale Structures of the Universe, is a very weak signal to detect. Nevertheless, even a very small amount can reveal a great deal about the origins of the Universe. It is therefore essential to be creative about the way we are going to compress the great amount of information coming from galaxy distributions (and other observables) in order to detect it.

Besides its importance, it is also the perfect parameter for our challenge: it is either there, or not. And the challenge is really about finding it, no matter what type, nor what amplitude it has. 

The possible contaminants of this signal are many, and in this challenge we include only a few. 

1. Gravitational instability makes the distribution of matter, and consequently of tracers such as galaxies, non-Gaussian even if it had started off as Gaussian. In this way, the initial conditions are gradually washed away as the Universe evolves.

1. A second important contaminant, especially when dealing with analysis tools that rely on simulations, is cosmic variance, i.e. the fact that we cannot know exactly the initial distribution of particles, be it Gaussian or not. 

1. Third, galaxies are observed in redshift space, not in real space. It means that the peculiar velocity of each galaxy slightly distorts the position of the galaxy along the line of sight. 

1. A fourth uncertainty is the relation between the underlying matter distribution and the galaxy distribution. For the challenge, we model it through the HOD algorithm, allowing the parameters of the model to vary in two of the three challenges. 

### Access to data

The catalogs for the challenge is found [here](https://mega.nz/folder/jMhQ3bBJ#sF5AJIa22Vj_C8zGOQLCow). The folder contains:

- The linear power spectrum file used to run the simulations, `CAMB_matterpow_0.5.dat`, which is composed of two columns $`k (h/`$Mpc. $`)`$ Pk $`({\rm Mpc}/h)^3`$

- A folder named `galaxy_catalogs`. Inside it, you can find 500 files named `runY.npz`, where Y runs from 0 to 499. The `npz` is the binary compressed format generated by Python. The catalogs can be loaded in Python with the following script:

``````
import numpy as np

cat = np.load('runY.npz')
pos = cat['pos']        # shape: (N_galaxies, 3) --> X,Y,Z position of each galaxy in Mpc/h
vel = cat['vel']        # shape: (N_galaxies, 3) --> Vx, Vy, Vz velocity of the galaxy in km/s
gtype = cat['gtype']    # shape: scalar --> Type of galaxy, central: 1, satellite: 0
``````
- The fiducial cosmology in each simulations is: 

  * $`\Omega_m = 0.3175`$
  * $`\Omega_b = 0.049`$
  * $`\Omega_\Lambda = 0.6825`$
  * $`h=0.6711`$
  * $`n_s = 0.9624`$
  * $`\sigma_8 = 0.834`$


**Note that, since velocities are given, you may reverse redshift space to real space for testing on a simpler setup.**

### First Challenge

A set of 500 galaxy catalogs is given. Each simulation is run with a different random seed in the initial conditions. Each of these catalogs has been generated using the fiducial HOD parameters. All runs have been downsampled to have the same number density. One of the catalogs has been run with  equilateral type non-Gaussianity with $`f_{\rm NL}^{\rm equi}=-100`$, `ENGm100L`. 

> The goal of the challenge is to identify the catalog that has been run with non-Gaussian initial conditions.


## Postprocessing

On the same folder where the catalogs are found, we include a few summary statistics to play with for the challenge.

### Power spectrum

We measure the redshift space galaxy monopole, quadrupole and hexadecapole power spectrum. Measurements of the galaxy density field are performed on a grid of linear size $`N=144^3`$ using a 4th-order mass assignment scheme and the interlacing technique to reduce aliasing (see [Sefusatti et al. 2016](https://arxiv.org/pdf/1512.07295.pdf)). We choose the k-bin size to be $`\Delta k = 2 k_f `$ , i.e., equal to the fundamental frequency of the simulation box $`k_f = 2\pi/L = 0.00628 h/`$Mpc. Note that the Fourier convention is 

```math
\delta(\mathbf{k}) = \int \frac{{\rm d}^3 x}{(2\pi)^3} e^{-i \mathbf{k}\cdot\mathbf{x}} \delta(\mathbf{x}).
```

The structure of the files is: 

``````
$`k`$ $`k_{\rm avg}`$  $`P_0`$  $`P_2`$  $`P_4`$  $`N_{\rm modes}`$
``````
where $`k_{\rm avg}`$ is the value of $`k`$ inside a bin averaged over the bin. On the third line of each file, you can find two numbers, corresponding to the number of galaxies for this catalog, and the related Poisson shot-noise.

### Bispectrum

We measure the redshift spave galaxy monopole bispectrum on the same density fields generated for the power spectrum. This setup amounts to 1655 triangles up to $`k_{\rm max} \simeq 0.3 h/$Mpc.

The structure of the files is: 

``````
$`k_1/k_f`$ $`k_2/k_f`$  $`k_3/k_f`$ $`P(k_1)`$ $`P(k_2)`$  $`P(k_3)`$  $`B(k_1,k_2,k_3)-{\rm SN}`$  $`B(k_1,k_2,k_3)`$  $`N_{\rm tr}`$,
``````
where $`{\rm SN} = 1/\bar n^2 + (P(k_1) + P(k_2) + P(k_3)) / \bar{n} `$ is the bispectrum Poisson shot-noise and $`N_{\rm tr}`$ is the number of triangles in a give triangle bin.
